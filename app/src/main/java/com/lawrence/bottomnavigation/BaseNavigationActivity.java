package com.lawrence.bottomnavigation;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

public abstract class BaseNavigationActivity extends AppCompatActivity {
    public static final String EXTRA_SELECTED_TAB = "selected_tab";
    private AHBottomNavigation bottomNavigation;

    @Override
    protected void onStart() {
        super.onStart();
        bottomNavigation.setCurrentItem(getIntent().getIntExtra(EXTRA_SELECTED_TAB, 0));
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(R.layout.activity_base_nav);

        createNavDrawer();

        FrameLayout content = (FrameLayout) findViewById(R.id.content_container);
        getLayoutInflater().inflate(layoutResID, content, true);
    }
    public void btnclick(View view) {
        TextView mText = (TextView) findViewById(R.id.text);
        mText.setText(mText.getText() + "+");
    }

    public void newActivity(View view) {
        Intent intent = new Intent(this, ChildActivity.class);
        intent.putExtra(EXTRA_SELECTED_TAB, getIntent().getIntExtra(EXTRA_SELECTED_TAB, 0));
        startActivity(intent);
    }



    private void createNavDrawer() {
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_nav);

        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, R.drawable.ic_search_black_24dp, R.color.color_tab_1);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, R.drawable.ic_favorite_border_black_24dp, R.color.color_tab_2);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, R.drawable.ic_create_black_24dp, R.color.color_tab_3);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.tab_4, R.drawable.ic_access_time_black_24dp, R.color.color_tab_4);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.tab_5, R.drawable.ic_account_circle_black_24dp, R.color.color_tab_5);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

        // Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

        // Change colors
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        // Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

        // Force the titles to be displayed (against Material Design guidelines!)
        bottomNavigation.setForceTitlesDisplay(true);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(false);

        // Set current item programmatically
        int currentTab = getIntent().getIntExtra(EXTRA_SELECTED_TAB, 0);
        bottomNavigation.setCurrentItem(currentTab);

        // Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));

        // Add or remove notification for each item
        bottomNavigation.setNotification(4, 1);
        bottomNavigation.setNotification(0, 1);

        // Set listener
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public void onTabSelected(final int position, boolean wasSelected) {
                if (wasSelected) return;

                final Class clazz;
                switch (position) {
                    case 0:
                        clazz = SearchActivity.class;
                        break;
                    case 1:
                        clazz = SavesActivity.class;
                        break;
                    case 2:
                        clazz = WriteReviewActivity.class;
                        break;
                    case 3:
                        clazz = TimelineActivity.class;
                        break;
                    case 4:
                        clazz = MeActivity.class;
                        break;
                    default:
                        clazz = SearchActivity.class;
                }

//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
                        Intent intent = new Intent(BaseNavigationActivity.this, clazz);
                        intent.putExtra(EXTRA_SELECTED_TAB, position);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(0,0);
//                    }
//                }, 300);
            }
        });
    }
}
